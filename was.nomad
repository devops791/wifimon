variable "reg_u" {
  type = string
  default = "xxx"
}
variable "reg_p" {
  type = string
  default = "xxx"
}

variable "reg_sa" {
  type = string
  default = "xxx"
}

variable "commit" {
  type = string
  default = "latest" 
}

job "was" {
    datacenters = ["dc1"]
    type = "service"
    // https://github.com/hashicorp/nomad/issues/3831
    group "ui" {
        count = 1
        constraint {
            attribute = "${attr.cpu.arch}"
            operator = "="
            value     = "arm64"
        }
        network {
            port "ui" {
                to = 8441
            }
        }
        service {
            name = "wifimon"
            port = "ui"
      	    tags = [
                "traefik.enable=true",
                "traefik.http.routers.wifimon.rule=Host(`wifimon.service.consul`)"
      	    ]
            check {
                type     = "http"
                port     = "ui"
                path     = "/"
                method   = "GET"
                interval = "30s"
                timeout  = "5s"
                check_restart {
                    limit = 3
                    grace = "120s"
                    ignore_warnings = false
                }
            }
        }
        task "wait for postgres" {
            lifecycle {
                hook = "prestart"
                sidecar = false
            }
            template {
                data = <<EOH
echo "getting script"
wget -q -O ${NOMAD_ALLOC_DIR}/wait-for-it.sh https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh

echo "fixing permissions"
chmod u+x ${NOMAD_ALLOC_DIR}/wait-for-it.sh

${NOMAD_ALLOC_DIR}/wait-for-it.sh --strict -h postgres.service.consul -p 5432 -t 300 -- echo "postgres is up"
                EOH
                destination = "setup.sh"
                perms = "0744"
            }

            driver = "raw_exec"
            config {
                command = "/bin/bash"
                args = ["setup.sh"]
            }
        }
        task "up" {
            driver = "docker"
            config {
                image = "registry.gitlab.com/devops791/wifimon/was-server:${var.commit}"
                auth {
                    username = var.reg_u
                    password = var.reg_p
                    server_address = var.reg_sa
                }
		        ports = ["ui"]
                command = "/usr/local/openjdk-11/bin/java"
                args = [
                    "-jar",
                    "/usr/lib/wifimon/ui-1.2.1.war",
                    "--spring.config.location=classpath:/usr/lib/wifimon/config/ui.properties,file:/usr/lib/wifimon/config/ui.properties"
                ]
            }
            resources {
                cpu    = 1024
                memory = 1024
            }
        } 

    }
    group "agent" {
        count = 1
        constraint {
            attribute = "${attr.cpu.arch}"
            operator = "="
            value     = "arm64"
        }
        network {
            port "agent" {
                static = 9000
                to = 9000
            }
        }
        service {
            name = "agent"
            port = "agent"
      	    tags = []
            check {
                type     = "http"
                port     = "agent"
                path     = "/wifimon/subnet"
                method   = "POST"
                interval = "30s"
                timeout  = "5s"
                check_restart {
                    limit = 3
                    grace = "120s"
                    ignore_warnings = false
                }
            }
        }
        task "up" {
            driver = "docker"
            config {
                image = "registry.gitlab.com/devops791/wifimon/was-server:${var.commit}"
                auth {
                    username = var.reg_u
                    password = var.reg_p
                    server_address = var.reg_sa
                }
		        ports = ["agent"]
                command = "/usr/local/openjdk-11/bin/java"
                args = [
                    "-jar",
                    "/usr/lib/wifimon/secure-processor-1.2.1.war",
                    "--spring.config.location=classpath:/usr/lib/wifimon/config/secure-processor.properties,file:/usr/lib/wifimon/config/secure-processor.properties"
                ]
            }
            resources {
                cpu    = 1024
                memory = 1024
            }
        } 

    }
}
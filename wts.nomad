variable "reg_u" {
  type = string
}
variable "reg_p" {
  type = string
}

variable "reg_sa" {
  type = string
}

variable "commit" {
  type = string
  default = "latest"
}

job "wts" {
    datacenters = ["dc1"]
    type = "service"

    group "server" {
        count = 1
        constraint {
            attribute = "${attr.cpu.arch}"
            operator = "="
            value     = "arm64"
        }
        network {
            port "http" {
                to = 80
            }
            port "https" {
                to = 443
            }
        }
        service {
            name = "wts"
            port = "http"
      	    tags = [
                "traefik.enable=true",
                "traefik.http.routers.wts.rule=Host(`wts.service.consul`)"
      	    ]
            check {
                type     = "http"
                port     = "http"
                path     = "/"
                method   = "GET"
                interval = "30s"
                timeout  = "5s"
                check_restart {
                    limit = 3
                    grace = "90s"
                    ignore_warnings = false
                }
            }

        }
        service {
            name = "wts-https"
            port = "https"
        }
        task "up" {
            driver = "docker"
            config {
                image = "registry.gitlab.com/devops791/wifimon/wts-server:${var.commit}"
                force_pull = true
                auth {
                    username = var.reg_u
                    password = var.reg_p
                    server_address = var.reg_sa
                }
		        ports = ["http"]
            }
        }
    }
}

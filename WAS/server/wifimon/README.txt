*********************************************
The necessary guides to install WiFiMon are included in the GEANT Wiki (https://wiki.geant.org)

Navigate to the GN4-3 Work Package 6 Workspace --> Task 3 --> WiFiMon

WiFiMon Guides are included in WiFiMon as a Service --> WiFiMon Service Support --> WiFiMon User Documentation

*********************************************

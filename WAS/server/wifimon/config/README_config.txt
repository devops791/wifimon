*********************************************
APPLICATION PROPERTY FILES
*********************************************
This folder contains the Spring Boot Application property files
(i.e. the configuration files) that are necessary to run WiFiMon.
You will have to fill in the values according to your setup

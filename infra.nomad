variable "reg_u" {
  type = string
  default = "xxx"
}
variable "reg_p" {
  type = string
  default = "xxx"
}

variable "reg_sa" {
  type = string
  default = "xxx"
}

variable "commit" {
  type = string
  default = "latest" 
}


job "infra" {
    datacenters = ["dc1"]
    type = "service"
    // https://github.com/hashicorp/nomad/issues/3831
    group "elasticsearch" {
        count = 1
        constraint {
            attribute = "${attr.unique.hostname}"
            operator = "="
            value     = "pi4"
        }
        network {
            port "http" {
                static = 9200
                to = 9200
            }
        }
        service {
            name = "elasticsearch"
            port = "http"
      	    tags = []
            check {
                type     = "http"
                port     = "http"
                path     = "/"
                method   = "GET"
                interval = "30s"
                timeout  = "5s"
                check_restart {
                    limit = 3
                    grace = "90s"
                    ignore_warnings = false
                }
            }
        }
        task "setup" {
            lifecycle {
                hook = "prestart"
                sidecar = false
            }
            driver = "raw_exec"
            config {
                command = "/usr/sbin/sysctl"
                args = ["-w","vm.max_map_count=262144"]
            }
        }
        task "up" {
            driver = "docker"
            config {
                image = "docker.elastic.co/elasticsearch/elasticsearch:7.9.3"
		        ports = ["http"]
                ulimit {
                    # ensure elastic search can lock all memory for the JVM on start
                    memlock = "-1"
                    # ensure elastic search can create enough open file handles
                    nofile = "65536"
                    # ensure elastic search can create enough threads
                    nproc = "8192"
                }  
            }
            // env = {
            //     "node.name" = "es01"
            //     "cluster.name" = "es-wifimon-cluster"
            //     "cluster.initial_master_nodes" = "es01"
            //     "bootstrap.memory_lock" = "true"
            //     "ES_JAVA_OPTS" = "-Xms512m -Xmx512m"     
            // }
            template {
                destination = "local/env"
                env         = true
                data        = <<EOH
node.name=es01
cluster.name=es-wifimon-cluster
cluster.initial_master_nodes=es01
bootstrap.memory_lock=true
ES_JAVA_OPTS="-Xms512m -Xmx512m"
                EOH
            }
        }
    }
    group "kibana" {
        count = 1
        constraint {
            attribute = "${attr.unique.hostname}"
            operator = "="
            value     = "DESKTOP-5BJ6ETO"
        }
        network {
            port "http" {
                static = 5601
                to = 5601
            }
        }
        service {
            name = "kibana"
            port = "http"
      	    tags = []
            check {
                type     = "http"
                port     = "http"
                path     = "/"
                method   = "GET"
                interval = "30s"
                timeout  = "5s"
                check_restart {
                    limit = 3
                    grace = "90s"
                    ignore_warnings = false
                }
            }
        }
        task "up" {
            driver = "docker"
            config {
                image = "registry.gitlab.com/devops791/wifimon/was-kibana:${var.commit}"
                auth {
                    username = var.reg_u
                    password = var.reg_p
                    server_address = var.reg_sa
                }
		        ports = ["http"]
            }
            resources {
                cpu    = 2048
                memory = 2048
            }
        }
    }
    group "postgres" {
        count = 1
        constraint {
            attribute = "${attr.unique.hostname}"
            operator = "="
            value     = "pi4"
        }
        network {
            port "tcp" {
                static = 5432
                to = 5432
            }
        }
        service {
            name = "postgres"
            port = "tcp"
      	    tags = []
            check {
                type     = "tcp"
                port     = "tcp"
                interval = "30s"
                timeout  = "5s"
                check_restart {
                    limit = 3
                    grace = "90s"
                    ignore_warnings = false
                }
            }
        }
        task "up" {
            driver = "docker"
            config {
                image = "registry.gitlab.com/devops791/wifimon/was-postgres:${var.commit}"
                auth {
                    username = var.reg_u
                    password = var.reg_p
                    server_address = var.reg_sa
                }
		        ports = ["tcp"]
            }
            env {
                POSTGRES_USER = "wifimon"
                POSTGRES_PASSWORD = "wifimon"    
            }
        }     
    }
}
CREATE USER wifimon_user WITH PASSWORD 'wifimonpass';
CREATE DATABASE wifimon_database OWNER wifimon_user;

\c wifimon_database;

CREATE TABLE subnets (
    subnet text,
    subnet_id serial PRIMARY KEY
);

CREATE TABLE accesspoints (
    apid serial PRIMARY KEY,
    mac text NOT NULL,
    latitude text,
    longitude text,
    building text,
    floor text,
    notes text
);

CREATE TABLE users (
    id serial PRIMARY KEY,
    email text NOT NULL,
    password_hash text NOT NULL,
    role text NOT NULL
);


CREATE TABLE options (
    optionsid serial PRIMARY KEY,
    userdata text NOT NULL,
    uservisualoption text NOT NULL,
    correlationmethod text NOT NULL
);


GRANT USAGE ON SCHEMA public to wifimon_user;
GRANT CONNECT ON DATABASE wifimon_database to wifimon_user;
 
\c wifimon_database
 
GRANT USAGE ON SCHEMA public to wifimon_user;
GRANT SELECT ON subnets, users, accesspoints, options TO wifimon_user;
GRANT INSERT ON subnets, users, accesspoints, options TO wifimon_user;
GRANT DELETE ON subnets, users, accesspoints, options TO wifimon_user;
GRANT UPDATE ON accesspoints, options TO wifimon_user;
GRANT USAGE, SELECT ON SEQUENCE subnets_subnet_id_seq TO wifimon_user;
GRANT USAGE, SELECT ON SEQUENCE users_id_seq TO wifimon_user;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE options_optionsid_seq TO wifimon_user;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE accesspoints_apid_seq TO wifimon_user;

\c wifimon_database
 
INSERT INTO users VALUES ('1', 'admin@test.com', '$2a$06$AnM.QevGa4BPGg7hc3nEBua6stnbZ8h4PrCjSbDxW.LWL7t4MX8vO', 'ADMIN');